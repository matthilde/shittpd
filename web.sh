#!/bin/bash
#
# Web server in bash
#


_get_error_message () {
    case "$1" in
        200)    printf "OK" ;;
        301)    printf "Moved Permanently" ;;
        302)    printf "Found" ;;
        400)    printf "Bad Request" ;;
        403)    printf "Forbidden" ;;
        404)    printf "Not Found" ;;
        405)    printf "Method Not Allowed" ;;
        500)    printf "Internal Server Error" ;;
        *)      printf "Unknown Status Code" ;;
    esac
}

_throw_error () {
    printf "HTTP/1.0 %s %s\r\nContent-Type: text/html\r\n\r\n" "$1" \
        "$(_get_error_message $1)"
    cat << EOF
<html>
    <body>
        <center><h1>$1 $(_get_error_message $1)</h1></center>
        <hr>
        <center>shittpd ${_server_version}</center>
    </body>
</html>
EOF
}

_get_content_type () {
    case "$1" in
        *.html)     printf "text/html" ;;
        *)          file --mime-type -b "$1" ;;
    esac
}

_serve_cgi () {
    SCRIPT_NAME=$(printf "$2" | cut -d '?' -f 1)
    SCRIPT_FILENAME=$webroot$SCRIPT_NAME
    QUERY_STRING=$(printf "$2" | cut -d '?' -f 2)
    [ -f "$SCRIPT_FILENAME" ] || {
        _throw_error 404
        return 1
    }
    
    [ "$QUERY_STRING" = "$SCRIPT_NAME" ] && QUERY_STRING=""

    QUERY_STRING=$QUERY_STRING \
        SCRIPT_NAME=$SCRIPT_NAME \
        SCRIPT_FILENAME=$SCRIPT_FILENAME \
        REQUEST_METHOD="$1" \
        REQUEST_URI="$2" \
        SERVER_SOFTWARE="shittpd/$_server_version" \
        SERVER_PROTOCOL="$3" \
        DOCUMENT_ROOT="$webroot" \
        GATEWAY_INTERFACE="CGI/1.1" \
        $SCRIPT_FILENAME

}

_serve_file () {
    [ -f "$1" ] || {
        _throw_error 404
        return 1
    }
    printf "HTTP/1.0 200 OK\r\nContent-type: %s\r\n\r\n" \
           "$(_get_content_type $1)"
    cat "$1"
}

# Usage: _generate_header STATUSCODE MIMETYPE
_generate_header () {
    printf "HTTP/1.0 %s\r\nContent-Type: %s\r\n\r\n" "$1" "$2"
}

# Usage: _serve_debug PATH
_serve_debug () {
    shift 1
    
    case "$1" in
        error)
            _throw_error $2
            ;;
        serverinfo)
            _generate_header "200 OK" "text/html"
            cgi_enabled=disabled
            [ "$ENABLE_CGI" = "1" ] && cgi_enabled=enabled
            cat << EOF
<html>
    <body>
        <h1>shittpd ${_server_version}</h1>
        <hr />
        <p>This is a really wacky web server.</p>
        <p>Debug mode is obviously enabled, I mean you are viewing this page
           rn</p>
        <p>CGI scripts are ${cgi_enabled}.</p>
    </body>
</html>
EOF
            ;;
        *)
            _generate_header "200 OK" "text/plain"
            printf "Debug mode space\r\nCommand: %s\r\n" "$*"
            ;;
    esac
}

handle_request () {
    read -r header
    set -- $(printf "$header" | sed -e 's/\r//g')
    
    case "$3" in
        HTTP/*) ;;
        *)
            _throw_error 400
            return 1
            ;;
    esac

    # Preventing directory traversal kek
    case "$2" in
        */..*)
            _throw_error 400
            return 1
            ;;
        /dbg/*)
            if [ "$ENABLE_DEBUG" = "1" ]; then
                args=$(printf "$2" | tr '/' ' ')
                _serve_debug $args
                return 0
            fi
            ;;
        /cgi-bin/*)
            if [ "$ENABLE_CGI" = "1" ]; then
                _serve_cgi "$1" "$2" "$3" || return 1
                return 0
            else
                ffile="$2"
            fi
            ;;
        /)  ffile="/index.html" ;;
        /*) ffile="$2"          ;;
        *)
            _throw_error 400
            return 1
            ;;
    esac
    [ "$1" = "GET" ] || {
        _throw_error 405
        return 1
    }

    _serve_file "$webroot$ffile"
}

# Usage: _serve_forever PORT 
_serve_forever () {
    port=$1
    
    while :; do
        ncat -l -i 5 -e "$0 -r" -p "$port"
    done
}

main () {
    [ "$1" ] || {
        printf "USAGE: $0 PORT\n"
        printf "USAGE: $0 -r\n"
        exit 1
    }
    if [ "$1" = "-r" ]; then
        handle_request
    else
        _serve_forever "$1"
    fi
}
webroot=${WEBROOT:-"./http"}
port=${PORT:-"6969"}
_server_version="1.0.0"

if [ "$1" = "-r" ]; then
    main -r
else
    [ "$1" ] && [ "$1" = "-r" ] || source "$1" || exit 1
    main "$port"
fi
