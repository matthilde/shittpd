     _     _ _   _             _ 
 ___| |__ (_) |_| |_ _ __   __| |
/ __| '_ \| | __| __| '_ \ / _` | HTTP web server written in Bash
\__ \ | | | | |_| |_| |_) | (_| | by Matthilde "~microwave"
|___/_| |_|_|\__|\__| .__/ \__,_|
                    |_|          

This is a webserver I have written in bash. It is the most stupid thing I have
ever created and I should be banned from shell scripting.

FEATURES
~~~~~~~~

 * Static serving
 * CGI scripts support (very wacky)
 * Spicy debug mode
 * Configuration file (wow)
 * Spaghetti code

CONFIGURATING AND RUNNING
~~~~~~~~~~~~~~~~~~~~~~~~~

Usage goes like this
    
    $ ./web.sh CONFIGFILE

An example config file is located in the repository with an obvious name.
If no config is specified, it will go for default configuration.

CGI
~~~

shittpd will run anything that is stored in /cgi-bin/ no matter what file it is.
If it can't run it, it will just shit itself. (TODO: fix that)

LICENSING
~~~~~~~~~

This work is licensed under the MIT License.
